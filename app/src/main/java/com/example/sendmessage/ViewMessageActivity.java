package com.example.sendmessage;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.TextView;

import com.example.sendmessage.model.Message;

/**
 * Esta clase recibe un mensaje de la activity otra actividad. La etiqueta de Javadoc @link
 * se utiliza en la definición de la clase o método. No después de las etiquetas @author ni @version.
 * En este caso se debe usar @see
 * Ver el método que crea el mensaje:
 * {@link SendMessageActivity#createMessage()}
 * @author Bilal Moreno
 * @version 1.0
 * @see SendMessageActivity
 * @see <a href="https://developer.android.com/reference/android/content/Context">Context</a>
 */
public class ViewMessageActivity extends AppCompatActivity {

    private static final String TAG = "com.example.sendmessage";
    private TextView tvSender, tvMessage;

    /**
     * Método onCreate del ciclo de vida de la Activity
     *
     * @param savedInstanceState Estado guardado de la Activity
     */
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_message);

        Log.d(TAG, "ViewMessage: onCreate");

        tvSender = findViewById(R.id.tvSender);
        tvMessage = findViewById(R.id.tvMessage);

        /*
            1. Recoger el Intent que ha enviado la actividad SendMessageActivity
         */
        Intent intent = getIntent();
        /*
            2. Recoger el objeto Bundle del Intent
         */
        Bundle bundle = intent.getExtras();
        /*
            3. Asignar los strings a sus componentes
         */
        Message message = (Message) bundle.getSerializable("message");
        tvSender.append(message.getAuthor());
        tvMessage.setText(message.getMessage());
        Log.d(TAG, "Autor: " + message.getAuthor() + ", Mensaje: " + message.getMessage());
    }

    /*
    Métodos callback: aquellos que son llamados siempre por el sistema operativo
    en el caso que se implementen.
     */
    @Override
    protected void onStart() {
        super.onStart(); //Por norma siempre se bede llamar al método padre.
        Log.d(TAG, "ViewMessage: onStart");
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.d(TAG, "ViewMessage: onResume");
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.d(TAG, "ViewMessage: onPause");
    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.d(TAG, "ViewMessage: onStop");
    }
}
