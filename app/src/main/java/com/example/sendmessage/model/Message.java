package com.example.sendmessage.model;

import java.io.Serializable;
import java.util.Objects;

/**
 * <p>Esta clase encapsula un mensaje y su autor en un objeto.</p>
 * @author Bilal
 * @version 1.0
 */
public class Message implements Serializable {
    private String author;
    private String message;

    /**
     * <p>Constructor vacío, que debe incluirse de forma explicita</p>
     */
    public Message() {

    }

    /**
     * <p>Método toString() sobrescrito.</p>
     * @return <p>Devuelve el Mensaje con el formato:</p>
     * <p>"Message{author=<i>autor</i>, message=<i>mensaje</i>}"</p>
     */
    @Override
    public String toString() {
        return "Message{" +
                "author='" + author + '\'' +
                ", message='" + message + '\'' +
                '}';
    }



    /**
     * <p>Autor del mensaje</p>
     * @return <p>Devuelve el nombre del autor</p>
     */
    public String getAuthor() {
        return author;
    }

    /**
     * <p>Define el autor del mensaje.</p>
     * @param author <p>Autor del mensaje.</p>
     */
    public void setAuthor(String author) {
        this.author = author;
    }

    /**
     * <p>Contenido del mensaje.</p>
     * @return <p>Devuelve el contenido del mensaje.</p>
     */
    public String getMessage() {
        return message;
    }

    /**
     * <p>Define el contenido del mensaje.</p>
     * @param message <p>Contenido del mensaje.</p>
     */
    public void setMessage(String message) {
        this.message = message;
    }

    /**
     * Determina si dos mensajes son iguales.
     * @param o Objeto a comparar.
     * @return
     */
    @Override
    public boolean equals(Object o) {
        //Si la referencia a ambos objetos es la misma.
        if (this == o) return true;
        //Si o es nulo o bien la clase de ambos objetos es diferente.
        if (o == null || getClass() != o.getClass()) return false;
        //Casting
        Message message1 = (Message) o;
        //Comprobar ambos atributos de ambos objetos, utilizando el método equals() de la
        //clase Objects
        return Objects.equals(author, message1.author) &&
                Objects.equals(message, message1.message);
    }

    @Override
    public int hashCode() {

        return Objects.hash(author, message);
    }
}
