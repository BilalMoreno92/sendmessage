package com.example.sendmessage;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.example.sendmessage.model.Message;

/**
 * Esta clase envía un mensaje de un usuario a otro
 * <p>En esta <b>unidad</b> vamos a prender los siguientes conceptos:</p>
 * <ul>
 * <li>Concepto de Context</li>
 * <li>Paso de parametros a través de un objeto <a target="_blank" href="https://developer.android.com/reference/android/os/Bundle">Bundle</a></li>
 * <li>Enviar un mensaje (Intent) entre dos Activity</li>
 * </ul>
 *
 * @author Bilal
 * @version 1.0
 * @see Message
 * @see android.os.Bundle
 *
 */
public class SendMessageActivity extends AppCompatActivity {

    private static final String TAG = "com.example.sendmessage";
    private EditText edAuthor, edMessage;
    private Button btSend;

    /**
     * Método onCreate del ciclo de vida de la Activity
     *
     * @param savedInstanceState Estado guardado de la Activity
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_send_message);

        Log.d(TAG, "SendMessage: onCreate");


        edAuthor = findViewById(R.id.edAuthor);
        edMessage = findViewById(R.id.edMessage);
        btSend = findViewById(R.id.btSend);
        /*
        1. Registrar un Listener OnClickListener
         */
        btSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.d(TAG, "Se ha pulsado el botón enviar");
                switch (view.getId()) {
                    case R.id.btSend:
                        createMessage();
                        break;
                }
            }
        });
    }

    /*
    Métodos callback: aquellos que son llamados siempre por el sistema operativo
    en el caso que se implementen.
     */
    @Override
    protected void onStart() {
        super.onStart(); //Por norma siempre se bede llamar al método padre.
        Log.d(TAG, "SendMessage: onStart");
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.d(TAG, "SendMessage: onResume");
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.d(TAG, "SendMessage: onPause");
    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.d(TAG, "SendMessage: onStop");
    }

    /**
     * <p>Este método se ejecutará siempre que se lance el evento onClick en la interfaz gráfica.</p>
     *
     * <p>(No implementado)</p>
     *
     * @param view View que ha lanzado el evento onClick.
     */
    public void getOnClick(View view) {
        Log.d(TAG, "Se ha pulsado el botón enviar");
        switch (view.getId()) {
            case R.id.btSend:
                createMessage();
                break;
        }
    }

    /**
     * <p>Crea y envia el mensaje encapsulando los datos en un objeto de la clase Message y
     * añadiendo este a un objeto Bundle que a su vez se añade al Intent que llamará a la Activity
     * que recibirá el mensaje.</p>
     */
    private void createMessage() {

        /*
           1. Crear un objeto contenedor o Bundle para añadir el objeto Message
            que tambien crearemos aquí.
        */
        Message message = new Message();
        message.setAuthor(edAuthor.getText().toString());
        message.setMessage(edMessage.getText().toString());
        Bundle bundle = new Bundle();
        bundle.putSerializable("message", message);

        /*
           2.Se crea el mensaje o Intent explicito. Se conoce la Activity de origen y la
           de destino.
        */
        Intent intent = new Intent(this, ViewMessageActivity.class);

        /*
           3. Añadir el objeto Bundle al Intent.
        */
        intent.putExtras(bundle);

        /*
           4. Iniciar la Activity destino ViewMessageActivity
        */
        startActivity(intent);
    }


}
